package a_handful_of_functions_for_reading_files

import (
	"bufio"
	"log"
	"os"
)

func OpenLines(filename string) []string {
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	var lines []string
	var line string

	for scanner.Scan() {
		line = scanner.Text()
		lines = append(lines, line)
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return lines
}

func OpenWords(filename string) []string {
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanWords)
	var words []string
	var word string

	for scanner.Scan() {
		word = scanner.Text()
		words = append(words, word)
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return words
}

func OpenBare(filename string) string {
	file, err := os.ReadFile(filename)
	if err != nil {
		log.Fatal(err)
	}
	content := string(file)
	return content
}
