package a_handful_of_functions_for_reading_files

import (
	"slices"
	"testing"
)

func TestOpenLines(t *testing.T) {
	real_input := OpenLines("testing_files/file_with_text.txt")
	expected_input := []string{"Jaro si k nám cestu probojovávalo.", "Slyšeli jste, jak se s ledy bilo?", "Dnes to z třešní ke mně zašeptalo,", "že se někde v boji poranilo.", "Krev že někde teče, že je někde rána",
		"nepozulíbaná, nezošetřovaná", "na mladičkém těle."}

	if slices.Compare(expected_input, real_input) != 0 {
		t.Errorf("Incorrect result! Got: %s, Expected: %s", real_input, expected_input)
	}
}

func TestOpenLinesEmpty(t *testing.T) {
	real_input := OpenLines("testing_files/empty_file.txt")
	expected_input := []string{}
	if slices.Compare(expected_input, real_input) != 0 {
		t.Errorf("Incorrect result! Got: %s, Expected: %s", real_input, expected_input)
	}
}

func TestOpenWords(t *testing.T) {
	real_input := OpenWords("testing_files/file_with_text.txt")
	expected_input := []string{"Jaro", "si", "k", "nám", "cestu", "probojovávalo.", "Slyšeli", "jste,", "jak", "se", "s", "ledy", "bilo?", "Dnes", "to", "z", "třešní", "ke", "mně", "zašeptalo,", "že", "se", "někde", "v", "boji", "poranilo.", "Krev", "že", "někde", "teče,", "že", "je", "někde", "rána",
		"nepozulíbaná,", "nezošetřovaná", "na", "mladičkém", "těle."}

	if slices.Compare(expected_input, real_input) != 0 {
		t.Errorf("Incorrect result! Got: %s, Expected: %s", real_input, expected_input)
	}
}

func TestOpenWordsEmpty(t *testing.T) {
	real_input := OpenWords("testing_files/empty_file.txt")
	expected_input := []string{}

	if slices.Compare(expected_input, real_input) != 0 {
		t.Errorf("Incorrect result! Got: %s, Expected: %s", real_input, expected_input)
	}
}

func TestOpenBare(t *testing.T) {
	real_input := OpenBare("testing_files/file_with_text.txt")
	expected_input := `Jaro si k nám cestu probojovávalo.
Slyšeli jste, jak se s ledy bilo?
Dnes to z třešní ke mně zašeptalo,
že se někde v boji poranilo.
Krev že někde teče, že je někde rána
nepozulíbaná, nezošetřovaná
na mladičkém těle.`

	if real_input != expected_input {
		t.Errorf("Incorrect result! Got: %s, Expected: %s", real_input, expected_input)
	}
}

func TestOpenBareEmpty(t *testing.T) {
	real_input := OpenBare("testing_files/empty_file.txt")
	expected_input := ""

	if real_input != expected_input {
		t.Errorf("Incorrect result! Got: %s, Expected: %s", real_input, expected_input)
	}
}
